package serveur;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class Serveur {


    public static void main(String[] args) throws RemoteException, MalformedURLException {
	// 0
	LocateRegistry.createRegistry(1099);
	
	// 1 instancier LDAPImpl
	LDAPImpl obj = new LDAPImpl();

	// 2 Naming
	Naming.rebind("rmi://localhost:1099/zuperdadape",obj);
    }

}
