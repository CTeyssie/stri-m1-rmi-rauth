package serveur;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import interfrmi.LDAP;
import utilities.CoupleExistantException;
import utilities.LDAP_PDU;

public class LDAPImpl extends UnicastRemoteObject implements LDAP {
    private HashMap<String,String> basecpt;
    
    public LDAPImpl() throws RemoteException {
	super();
	this.basecpt = new HashMap<>();
    }

    @Override
    public boolean tester(LDAP_PDU obj) throws RemoteException {
	if (this.basecpt.containsKey(obj.getLogin())) return this.basecpt.get(obj.getLogin()).equals(obj.getPassword());
	return false;
    }

    @Override
    public void ajouterCouple(LDAP_PDU obj) throws RemoteException, CoupleExistantException {
	if  (this.basecpt.containsKey(obj.getLogin())) throw new CoupleExistantException();
	else this.basecpt.put(obj.getLogin(),obj.getPassword());

    }
}
