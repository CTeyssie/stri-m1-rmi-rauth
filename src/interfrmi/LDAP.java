package interfrmi;

import java.rmi.RemoteException;

import utilities.CoupleExistantException;
import utilities.LDAP_PDU;

public interface LDAP extends java.rmi.Remote {

    public boolean tester(LDAP_PDU obj) throws RemoteException;
    public void ajouterCouple(LDAP_PDU obj) throws RemoteException, CoupleExistantException;
}
